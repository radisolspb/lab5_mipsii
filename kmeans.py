#! /bin/env python3

import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.metrics import accuracy_score
import json
import itertools
from sklearn.datasets import load_iris  # hehe not using in kmeans


class Lab4kmeans():

    def __init__(self, filename):
        try:
            self.datasource = open(filename, 'r')
        except TypeError:
            self.datasource = filename

        self.inertia_k = 0.8
        self.kmeans = None
        self.kmeans_optimal = None

    def read_data(self):
        self.iris = []
        self.iris_names = []

        for line in self.datasource.readlines():

            iris_vector = tuple(line.split(',')[:-1])
            if len(iris_vector):
                self.iris.append(iris_vector)
                self.iris_names.append(line.split(',')[-1].strip())
            # print(line)

        self.iris_types = list(set(self.iris_names))
        print(self.iris_types)

    def get_clusters(self, custom_length=None):

        if not custom_length:
            # Function for getting optimal numbler cluster elements
            inertias = []
            plot_len = 8

            for i in range(1, plot_len):
                kmeans = KMeans(n_clusters=i, verbose=0)
                kmeans.fit(self.iris)

                if i > 1:
                    new_inertia = float(kmeans.inertia_) / self.kmeans.inertia_
                    if new_inertia < self.inertia_k:
                        self.kmeans = kmeans
                    elif not self.kmeans_optimal:
                        prev_kmeans = KMeans(n_clusters=(i-2))
                        prev_kmeans.fit(self.iris)
                        self.kmeans_optimal = prev_kmeans
                else:
                    self.kmeans = kmeans

                inertias.append(kmeans.inertia_)

            plt.plot(range(1, plot_len), inertias, marker='o')
            plt.title('Elbow method')
            plt.xlabel('Number of clusters')
            plt.ylabel('Square distance')
            plt.savefig("elbow_graph.png")
            plt.close()
        else:
            # manually added number of clusters
            kmeans = KMeans(n_clusters=int(custom_length), verbose=0)
            kmeans.fit(self.iris)
            self.kmeans_optimal = kmeans

    def predict(self):

        # Naming
        #self.iris_type_2_cluster= list(set(self.kmeans_optimal.labels_))

        lib_predict = self.kmeans_optimal.predict(self.iris)
        model_labels = self.kmeans_optimal.labels_

        #print(lib_predict, model_labels)

        def_iris = load_iris()
        res_acc = 0

        for is_cluster in itertools.permutations([0, 1, 2]):
            gg = []
            for i in def_iris.target:
                gg.append(is_cluster[i])

            acc = accuracy_score(lib_predict, gg)

            if acc > res_acc:
                res_acc = acc

        predict = []
        txt_result = []

        dd_cluster = list(set(self.kmeans_optimal.labels_))
        named_cluster = dict(zip(dd_cluster, self.iris_names))
        model_labels = self.kmeans_optimal.labels_

        for idx, predict_val in enumerate(model_labels):
            txt_result.append(f"{self.iris_names[idx]},{predict_val}")

        with open('./data/kmeans_predict.txt', 'w') as f:
            f.write('\n'.join(txt_result))

        with open('./data/kmeans_metric.txt', 'w') as f:
            f.write(f'kmeans accuracy: {res_acc}\n')

        print(res_acc)
        return res_acc


def run(**kwargs):
    myLab4 = Lab4kmeans('data/iris.data')
    myLab4.read_data()
    # Draw Elbow
    myLab4.get_clusters()
    # Set number of clusters for 3
    myLab4.get_clusters(custom_length=3)
    return myLab4.predict()


if __name__ == '__main__':
    run()
