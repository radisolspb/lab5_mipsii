#! /bin/env python3

from sklearn.metrics import accuracy_score
from sklearn import tree, ensemble
from sklearn.datasets import load_iris  # hehe not using in kmeans
from sklearn.model_selection import cross_val_score
import json


class Lab4dtree():

    def __init__(self, filename, max_depth: int = 3):
        try:
            self.datasource = open(filename, 'r')
        except TypeError:
            self.datasource = filename

        self.inertia_k = 0.8
        self.kmeans = None
        self.kmeans_optimal = None
        self.max_depth = max_depth

    def read_data(self):
        self.iris = []
        self.iris_names = []

        for line in self.datasource.readlines():

            iris_vector = tuple(line.split(',')[:-1])
            if len(iris_vector):
                self.iris.append(iris_vector)
                self.iris_names.append(line.split(',')[-1].strip())

        self.iris_types = list(set(self.iris_names))
        self.iris_target = []

        for nm in self.iris_names:
            self.iris_target.append(self.iris_types.index(nm))

    def build_dtree(self):
        clf = tree.DecisionTreeClassifier(max_depth=self.max_depth)
        self.dtree = clf.fit(self.iris, self.iris_target)

    def build_random_forest(self):
        clf = ensemble.RandomForestClassifier(max_depth=self.max_depth)
        self.dtree = clf.fit(self.iris, self.iris_target)
        self.dtree.score(self.iris, self.iris_target)
        return True

    def predict(self, file_prefix: str):

        predict = []
        txt_result = []

        for idx, iris in enumerate(self.iris):
            predict.append(self.dtree.predict([iris, ])[0])
            txt_result.append(f"{self.iris_names[idx]}, {predict[-1]}")

        with open(f'./data/{file_prefix}_predict.txt', 'w') as f:
            f.write('\n'.join(txt_result))

        acc = accuracy_score(self.iris_target, predict)
        with open(f'./data/{file_prefix}_metric.txt', 'w') as f:
            f.write(f'{file_prefix} accuracy: {acc}\n')
        print(acc)
        return(acc)


def run(**kwargs):
    max_depth = kwargs.get('max_depth', 3)

    myLab4 = Lab4dtree('data/iris.data')
    myLab4.read_data()
    myLab4.build_dtree()
    return myLab4.predict('dtree')


if __name__ == '__main__':
    run()
