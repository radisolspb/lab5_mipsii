#! /bin/env python3

from dtree import Lab4dtree
import dtree
import kmeans
import rtree
import matplotlib.pyplot as plt


if __name__ == '__main__':
    plot_len = ['kmeans', 'rtree', 'dtree']
    vals = []
    depth = 4

    for al in plot_len:
        i = __import__(al)
        vals.append(i.run(max_depth=depth))

    fig = plt.figure()
    ax = fig.add_subplot()
    plt.plot(plot_len, vals, marker='o')

    for i, j in zip(plot_len, vals):
        ax.annotate(str(j), xy=(i, j))

    plt.title('Algorithm comparation')
    plt.xlabel('Algorythm name')
    plt.ylabel('Accuracy')
    plt.savefig("result_graf.png")
