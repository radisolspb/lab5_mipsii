#! /bin/env python3

from dtree import Lab4dtree


def run(**kwargs):

    max_depth = kwargs.get('max_depth', 3)

    myLab4 = Lab4dtree('data/iris.data', max_depth)
    myLab4.read_data()
    myLab4.build_random_forest()
    return(myLab4.predict('rtree'))


if __name__ == '__main__':
    run()
